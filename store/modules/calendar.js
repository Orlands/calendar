import Vue from 'vue'

export default {
  namespaced: true,

  state: {
    events: [
      { id: 2, start: '2022-03-29T10:30:00+01:00', end: '2022-03-29T14:30:00+01:00', title: 'test1'},
      { id: 1, start: '2022-03-30T06:30:00+01:00', end: '2022-03-31T09:30:00+01:00', title: 'test2'}
    ]
  },

  getters: {
    getEvents(state) {
      return state.events
    }
  },

  mutations: {
    addEventToState(state, newEvent) {
      state.events.push(newEvent)
    },
    updateEventToState(state, event) {
      const index = state.events.findIndex(e => e.id === event.id)
      Vue.set(state.events, index, event)
    },
    deleteEventInState(state, id) {
      const index = state.events.findIndex(event => event.id === id);
      state.events.splice(index, 1)
    }
  },

  actions: {
    addEvent({ commit }, data) {
          const newEvent = {
            start: data.startStr,
            end: data.endStr,
            title: data.title,
            id: Math.floor(Math.random() * 100)
          }
          if (new Date(newEvent.end).getTime() > new Date(newEvent.start).getTime()) {
            commit('addEventToState', newEvent)
            return '200'
          }
          return '500'
    },
    updateEvent({ commit }, data) {
          const newEvent = {
            start: data.startStr,
            end: data.endStr,
            title: data.title,
            id: parseInt(data.id)
          }
          if (newEvent.end > newEvent.start ) {
            commit('updateEventToState', newEvent)
            return '200'
          }
          return '500'
    },
    deleteEvent({commit}, id) {
      commit('deleteEventInState', id)
      return '200'
    }
  }
}