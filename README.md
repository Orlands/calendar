# Calendar

## About the project
This is a project I made using Fullcalendar, it allows me to plan my schedule according to different events.

## Usage

The integration of this code snippet can be displayed on back office. It will let the users to have a view of his schedule during the week / month.


### Built With

* [Nuxt.js](https://nuxtjs.org/)
* [Vue.js](https://vuejs.org/)
* [Bootstrap-vue](https://bootstrap-vue.org/)

## Getting Started

### Installation

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

